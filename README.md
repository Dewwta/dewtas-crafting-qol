# Dewta's Crafting QOL!
## This Mod Was created by Dewta!

This mod is just a small collection of recipe edits that make the game more tolerable in my preference, other people might feel the same way, which is why im posting it.

## Support

Our discord server will be open to support for our mods and our mods only, we do not answer support requests that ask us to Add compatibility with other mods. We only design our modpacks with our mods in mind.
**https://discord.gg/9rxDH9EYjs**

## Delevopment

I am a mod developer who just likes making mods for fun

**Development/UI Design** - Dewta

If you would like to support me my link will be below, donating is not required, just helps me make more mods. Thank you! <br>
<a href="https://www.paypal.com/paypalme/Dellta55">Paypal</a>


## License

If you modify this work both authors must be credited. And you are not allowed to post this mod.

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US">Creative Commons Attribution 3.0 Unported License</a>.
